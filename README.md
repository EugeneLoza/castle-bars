# Castle Bars

Several basic bars (horizontal, vertical and circular progressbars) and sliders (horizontal, vertical and continuous, discrete).

Ready to be used in Casatle Editor. See (TODO "Using custom components" link) for details.

# License

License is the same as Castle Game Engine, unless explicitly specified, see https://castle-engine.io/license.php
