unit CastleSimpleProgressBar;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleGlImages, CastleUiControls, CastleClassUtils,
  CastleColors;

type
  TCastleSimpleProgressBar = class(TCastleUserInterface)
  strict private
    const
      DefaultValue = 0.5;
    var
      FValue: Single;
      FVertical: Boolean;
      FImage: TDrawableImage;
      FImageExists: Boolean;
      FColorPersistent: TCastleColorPersistent;
    function GetColor: TCastleColor;
    function GetColorForPersistent: TCastleColor;
    procedure SetColor(const AValue: TCastleColor);
    procedure SetColorForPersistent(const AValue: TCastleColor);
    function GetUrl: String;
    procedure SetUrl(const AValue: String);
    procedure SetValue(const AValue: Single);
    procedure SetVertical(AValue: Boolean);
  public
    { Progress bar color multiplier. By default, opaque white. }
    property Color: TCastleColor read GetColor write SetColor;
    function PropertySections(const PropertyName: String): TPropertySections; override;
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Url of the image of the bar }
    property Url: String read GetUrl write SetUrl;
    { If this progress bar is vertical }
    property Vertical: Boolean read FVertical write SetVertical;
    { Value in range 0..1 }
    property Value: Single read FValue write SetValue {$ifdef FPC}default DefaultValue{$endif};
    { Color multiplier for progressbar image
      This color will be multiplied by the image pixels }
    property ColorPersistent: TCastleColorPersistent read FColorPersistent ;
  end;

implementation
uses
  SysUtils,
  CastleVectors, CastleComponentSerialize, CastleImages
  {$ifdef CASTLE_DESIGN_MODE}, PropEdits, CastlePropEdits{$endif};

procedure TCastleSimpleProgressBar.SetValue(const AValue: Single);
begin
  FValue := AValue;
  if FValue >= 1.0 - Single.Epsilon then
  begin
    FImageExists := true;
    FImage.Clip := false;
  end else
  if FValue < Single.Epsilon then
    FImageExists := false
  else
  begin
    FImageExists := true;
    FImage.Clip := true;
    if Vertical then
      FImage.ClipLine := Vector3(0, -1, FValue)
    else
      FImage.ClipLine := Vector3(-1, 0, FValue);
  end;
end;

procedure TCastleSimpleProgressBar.SetVertical(AValue: Boolean);
begin
  if FVertical <> AValue then
  begin
    FVertical := AValue;
    SetValue(Value);
  end;
end;

function TCastleSimpleProgressBar.GetColor: TCastleColor;
begin
  Exit(FImage.Color);
end;

function TCastleSimpleProgressBar.GetColorForPersistent: TCastleColor;
begin
  Exit(Color);
end;

procedure TCastleSimpleProgressBar.SetColor(const AValue: TCastleColor);
begin
  FImage.Color := AValue;
end;

procedure TCastleSimpleProgressBar.SetColorForPersistent(
  const AValue: TCastleColor);
begin
  Color := AValue;
end;

function TCastleSimpleProgressBar.GetUrl: String;
begin
  Exit(FImage.URL);
end;

procedure TCastleSimpleProgressBar.SetUrl(const AValue: String);
begin
  FImage.URL := AValue;
end;

function TCastleSimpleProgressBar.PropertySections(const PropertyName: String): TPropertySections;
begin
  if (PropertyName = 'Value') or
     (PropertyName = 'Vertical') or
     (PropertyName = 'ColorPersistent') or
     (PropertyName = 'Url') then
    Result := [psBasic]
  else
    Result := inherited PropertySections(PropertyName);
end;

constructor TCastleSimpleProgressBar.Create(AOwner: TComponent);
var
  WhitePixel: TRgbImage;
begin
  inherited Create(AOwner);
  WhitePixel := TRgbImage.Create(1, 1);
  WhitePixel.Clear(Vector4Byte(1, 1, 1, 1));
  FImage := TDrawableImage.Create(WhitePixel, false, true);
  FColorPersistent := TCastleColorPersistent.Create;
  FColorPersistent.InternalGetValue := {$ifdef FPC}@{$endif}GetColorForPersistent;
  FColorPersistent.InternalSetValue := {$ifdef FPC}@{$endif}SetColorForPersistent;
  FColorPersistent.InternalDefaultValue := CastleColors.White;
  SetValue(DefaultValue);
end;

destructor TCastleSimpleProgressBar.Destroy;
begin
  FImage.Free;
  FColorPersistent.Free;
  inherited Destroy;
end;

procedure TCastleSimpleProgressBar.Render;
begin
  inherited Render;
  if FImageExists then
    FImage.Draw(RenderRect);
end;

initialization
  RegisterSerializableComponent(TCastleSimpleProgressBar, 'Progress Bar');
  {$ifdef CASTLE_DESIGN_MODE}
  RegisterPropertyEditor(TypeInfo(AnsiString), TCastleSimpleProgressBar, 'Url', TImageURLPropertyEditor);
  //RegisterPropertyEditor(TypeInfo(TCastleColorPersistent), TCastleSimpleProgressBar, 'Color', TCastleColorPropertyEditor); // No need, it will automatically get registered in ColorPersistent
  {$endif}
end.

