unit CastleCircularProgressBar;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleGlImages, CastleUiControls, CastleClassUtils,
  CastleVectors, CastleColors;

type
  { A Circular progress bar
    The value grows counter-clockwise
    Zero and max values are at the top }
  TCastleCircularProgressBar = class(TCastleUserInterface)
  strict private
    const
      DefaultValue = 0.5;
    var
      FUrl: String;
      FValue: Single;
      FImageLeftExists, FImageRightExists: Boolean;
      FImageLeft, FImageRight: TDrawableImage;
      FColorPersistent: TCastleColorPersistent;
    function GetColor: TCastleColor;
    function GetColorForPersistent: TCastleColor;
    procedure SetColor(const AValue: TCastleColor);
    procedure SetColorForPersistent(const AValue: TCastleColor);
    function GetUrl: String;
    procedure SetUrl(const AValue: String);
    procedure SetValue(const AValue: Single);
  public
    { Progress bar color multiplier. By default, opaque white. }
    property Color: TCastleColor read GetColor write SetColor;
    function PropertySections(const PropertyName: String): TPropertySections; override;
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Url of the circular image }
    property Url: String read GetUrl write SetUrl;
    { Value of the slider within 0..1 range }
    property Value: Single read FValue write SetValue {$ifdef FPC}default DefaultValue{$endif};
    { Color of the circular image
      Will be multiplied by the color of the image pixels }
    property ColorPersistent: TCastleColorPersistent read FColorPersistent ;
  end;

implementation
uses
  SysUtils, Math,
  CastleLog,
  CastleComponentSerialize, CastleImages, CastleRectangles
  {$ifdef CASTLE_DESIGN_MODE}, PropEdits, CastlePropEdits{$endif};

procedure TCastleCircularProgressBar.SetValue(const AValue: Single);
var
  Ta: Single;
begin
  FValue := AValue;

  if FValue >= 1.0 - Single.Epsilon then
  begin
    FImageLeftExists := true;
    FImageRightExists := true;
    FImageLeft.Clip := false;
    FImageRight.Clip := false;
  end else
  if Abs(FValue - 0.5) <= Single.Epsilon then
  begin
    FImageLeftExists := true;
    FImageRightExists := false;
    FImageLeft.Clip := false;
  end else
  if FValue <= Single.Epsilon then
  begin
    FImageLeftExists := false;
    FImageRightExists := false;
  end else
  if FValue > 0.5 then
  begin
    FImageLeftExists := true;
    FImageLeft.Clip := false;
    FImageRightExists := true;
    FImageRight.Clip := true;
    FImageRight.ClipLine := Vector3(Tan((FValue - 0.75) * Pi * 2) / 2, -1, 0.5);
  end else
  // < 0.5
  begin
    FImageLeftExists := true;
    FImageLeft.Clip := true;
    FImageRightExists := false;
    Ta := Tan((FValue - 0.25) * Pi * 2) / 2;
    FImageLeft.ClipLine := Vector3(-Ta, +1, -0.5 + Ta);
  end;
end;

function TCastleCircularProgressBar.GetColor: TCastleColor;
begin
  Exit(FImageLeft.Color);
end;

function TCastleCircularProgressBar.GetColorForPersistent: TCastleColor;
begin
  Exit(Color);
end;

procedure TCastleCircularProgressBar.SetColor(const AValue: TCastleColor);
begin
  FImageLeft.Color := AValue;
  FImageRight.Color := AValue;
end;

procedure TCastleCircularProgressBar.SetColorForPersistent(
  const AValue: TCastleColor);
begin
  Color := AValue;
end;

function TCastleCircularProgressBar.GetUrl: String;
begin
  Exit(FUrl);
end;

procedure TCastleCircularProgressBar.SetUrl(const AValue: String);
var
  Image, ImageLeft, ImageRight: TCastleImage;
  HalfWidth: Cardinal;
  OldColor: TCastleColor;
begin
  FUrl := AValue;
  Image := LoadImage(FUrl);
  if Image.Width mod 2 = 1 then
    WriteLnWarning('Image %s width=%d is not even, artifacts can occur', [FUrl, Image.Width]);
  HalfWidth := Image.Width div 2;
  ImageLeft := TRGBAlphaImage.Create(HalfWidth, Image.Height);
  ImageRight := TRGBAlphaImage.Create(HalfWidth, Image.Height);
  ImageLeft.DrawFrom(Image, 0, 0, 0, 0, HalfWidth, Image.Height, dmOverwrite);
  ImageRight.DrawFrom(Image, 0, 0, HalfWidth, 0, Image.Width - HalfWidth, Image.Height, dmOverwrite);
  Image.Free;
  OldColor := Color;
  FreeAndNil(FImageLeft);
  FreeAndNil(FImageRight);
  FImageLeft := TDrawableImage.Create(ImageLeft, true, true);
  FImageRight := TDrawableImage.Create(ImageRight, true, true);
  FImageLeft.Color := OldColor;
  FImageRight.Color := OldColor;
  SetValue(FValue);
end;

function TCastleCircularProgressBar.PropertySections(const PropertyName: String): TPropertySections;
begin
  if (PropertyName = 'Value') or
     (PropertyName = 'ColorPersistent') or
     (PropertyName = 'Url') then
    Result := [psBasic]
  else
    Result := inherited PropertySections(PropertyName);
end;

constructor TCastleCircularProgressBar.Create(AOwner: TComponent);
var
  WhitePixel: TRgbImage;
begin
  inherited Create(AOwner);
  WhitePixel := TRgbImage.Create(1, 1);
  WhitePixel.Clear(Vector4Byte(1, 1, 1, 1));
  FImageLeft := TDrawableImage.Create(WhitePixel, false, true);
  FImageRight := TDrawableImage.Create(WhitePixel, false, false);
  FUrl := '';
  FColorPersistent := TCastleColorPersistent.Create;
  FColorPersistent.InternalGetValue := {$ifdef FPC}@{$endif}GetColorForPersistent;
  FColorPersistent.InternalSetValue := {$ifdef FPC}@{$endif}SetColorForPersistent;
  FColorPersistent.InternalDefaultValue := CastleColors.White;
  SetValue(DefaultValue);
end;

destructor TCastleCircularProgressBar.Destroy;
begin
  FImageLeft.Free;
  FImageRight.Free;
  FColorPersistent.Free;
  inherited Destroy;
end;

procedure TCastleCircularProgressBar.Render;
var
  LeftRect, RightRect: TFloatRectangle;
begin
  inherited Render;
  // unfortunately RenderRect can change unexpectedly so we need to recalculate those every frame
  // Maybe we can reliably cache them in Resize?
  if FImageLeftExists then
  begin
    LeftRect := FloatRectangle(RenderRect.Left, RenderRect.Bottom, RenderRect.Width / 2, RenderRect.Height);
    FImageLeft.Draw(LeftRect);
  end;
  if FImageRightExists then
  begin
    RightRect := FloatRectangle(RenderRect.Left + RenderRect.Width / 2, RenderRect.Bottom, RenderRect.Width / 2, RenderRect.Height);
    FImageRight.Draw(RightRect);
  end;
end;

initialization
  RegisterSerializableComponent(TCastleCircularProgressBar, 'Circular Bar');
  {$ifdef CASTLE_DESIGN_MODE}
  RegisterPropertyEditor(TypeInfo(AnsiString), TCastleCircularProgressBar, 'Url', TImageURLPropertyEditor);
  //RegisterPropertyEditor(TypeInfo(TCastleColorPersistent), TCastleCircularProgressBar, 'Color', TCastleColorPropertyEditor); // No need, it will automatically get registered in ColorPersistent
  {$endif}
end.

