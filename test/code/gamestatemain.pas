{ Castle Bars and Sliders Example

  Copyright (c) 2022-2022 Yevhen Loza

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
}
unit GameStateMain;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse,
  CastleTimeUtils,

  CastleSimpleProgressBar, CastleCircularProgressBar, CastleSlider;

type
  { Main state, where most of the application logic takes place. }
  TStateMain = class(TUIState)
  SimpleProgressBarGreen: TCastleSimpleProgressBar;
  SimpleProgressBarYellow: TCastleSimpleProgressBar;
  VerticalProgressBarRed: TCastleSimpleProgressBar;
  VerticalProgressBarPurple: TCastleSimpleProgressBar;
  CircularProgressBarCyan: TCastleCircularProgressBar;
  CircularProgressBarBlue: TCastleCircularProgressBar;
  ButtonSliderLeft, ButtonSliderRight: TCastleButton;
  HorizontalFloatSlider: TCastleSlider;
  ButtonSliderUp, ButtonSliderDown: TCastleButton;
  VerticalDiscreteSlider: TCastleDiscreteSlider;
  strict private
    StartTimer: TTimerResult;
    procedure DoHorizontalSliderChanged(Sender: TObject);
    procedure DoSliderDown(Sender: TObject);
    procedure DoSliderLeft(Sender: TObject);
    procedure DoSliderRight(Sender: TObject);
    procedure DoSliderUp(Sender: TObject);
    procedure DoVerticalSliderChanged(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils, CastleColors;

{ TStateMain ----------------------------------------------------------------- }

procedure TStateMain.DoSliderLeft(Sender: TObject);
begin
  HorizontalFloatSlider.Value := HorizontalFloatSlider.Value - 0.2;
end;
procedure TStateMain.DoSliderRight(Sender: TObject);
begin
  HorizontalFloatSlider.Value := HorizontalFloatSlider.Value + 0.2;
end;
procedure TStateMain.DoHorizontalSliderChanged(Sender: TObject);
begin
  ButtonSliderLeft.Enabled := HorizontalFloatSlider.Value > Single.Epsilon;
  ButtonSliderRight.Enabled := HorizontalFloatSlider.Value < 1.0 - Single.Epsilon;
end;

procedure TStateMain.DoSliderDown(Sender: TObject);
begin
  VerticalDiscreteSlider.Value := VerticalDiscreteSlider.Value - 1.0 / VerticalDiscreteSlider.Steps;
end;
procedure TStateMain.DoSliderUp(Sender: TObject);
begin
  VerticalDiscreteSlider.Value := VerticalDiscreteSlider.Value + 1.0 / VerticalDiscreteSlider.Steps;
end;
procedure TStateMain.DoVerticalSliderChanged(Sender: TObject);
begin
  ButtonSliderDown.Enabled := VerticalDiscreteSlider.Value > Single.Epsilon;
  ButtonSliderUp.Enabled := VerticalDiscreteSlider.Value < 1.0 - Single.Epsilon;
end;

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gamestatemain.castle-user-interface';
end;

procedure TStateMain.Start;
begin
  inherited;
  StartTimer := Timer;
  ButtonSliderLeft.OnClick := @DoSliderLeft;
  ButtonSliderRight.OnClick := @DoSliderRight;
  HorizontalFloatSlider.OnChanged := @DoHorizontalSliderChanged;
  ButtonSliderUp.OnClick := @DoSliderUp;
  ButtonSliderDown.OnClick := @DoSliderDown;
  VerticalDiscreteSlider.OnChanged := @DoVerticalSliderChanged;
end;

procedure TStateMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  SimpleProgressBarGreen.Value := Sqr(Cos(StartTimer.ElapsedTime / 2.0));
  SimpleProgressBarYellow.Value := Sqr(Cos(StartTimer.ElapsedTime / 1.6 + 1));
  VerticalProgressBarRed.Value := Sqr(Cos(StartTimer.ElapsedTime / 2.1 + 3));
  VerticalProgressBarPurple.Value := Sqr(Cos(StartTimer.ElapsedTime / 1.7 + 4));
  CircularProgressBarCyan.Value := Sqr(Cos(StartTimer.ElapsedTime / 2.2 + 2));
  CircularProgressBarBlue.Value := Sqr(Cos(StartTimer.ElapsedTime / 1.8 + 5));
end;

end.
